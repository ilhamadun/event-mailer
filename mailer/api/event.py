# -*- coding: utf-8 -*-
"""
    mailer.api.event
    ~~~~~~~~~~~~~~~~

    Request handler for Event API.

    :copyright: © 2018 by Ilham Imaduddin.
"""
# pylint: disable=no-self-use

import re
from typing import Dict, List, Union

from flask import Response, request, jsonify
from peewee import PeeweeException
from playhouse.shortcuts import model_to_dict

from mailer.api.base import RequestData
from mailer.api.exception import RequestError
from mailer.models.event import Event


class EventPOSTData(RequestData):  # pylint: disable=too-few-public-methods
    """Validated data of the Event POST API."""

    schema = {
        "type": "object",
        "properties": {
            "recipients": {"type": "array", "items": {"type": "string"}},
            "category": {"type": "string"},
        },
        "required": ["recipients", "category"],
    }

    def __init__(self, data: Dict[str, str]) -> None:
        """Initialize EventPOSTData with validation.

        Arguments:
            data {Dict[str, str]} -- Raw HTTP request data

        """

        self.recipients: List[str] = []
        self.category = None

        self.load(data, request_name="event")
        self.__validate_email()

    def __validate_email(self):
        for email in self.recipients:
            match = re.match(
                "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$",  # pylint: disable=anomalous-backslash-in-string
                email,
            )

            if not match:
                raise RequestError(
                    400,
                    "event/validation-error",
                    "Recipient's email address is invalid.",
                )


class EventHandler(object):
    """Handle request to create, get and list Event."""

    @staticmethod
    def handle_request(event_id: int = None) -> Response:
        """Route a request, validate incoming data and return a response.

        Keyword Arguments:
            event_id {int} -- Used to get Event by id (default: {None})

        Returns:
            [Response] -- A flask.Response containing data from the routed methods.
        """

        try:
            handler = EventHandler()

            if request.method == "GET":
                if event_id:
                    response = handler.get(event_id)
                else:
                    response = handler.list()

            elif request.method == "POST":
                response = handler.create()

        except RequestError as err:
            response = err.response()

        return response

    def list(self) -> Response:
        """List all events

        Returns:
            Response -- A flask.Response containing the Event list
        """

        events = []
        for event in self.__list_event():
            events.append(model_to_dict(event))

        return jsonify(
            {
                "ok": True,
                "status": "event/list-success",
                "message": "Event has been successfully listed.",
                "events": events,
            }
        )

    def get(self, event_id: int) -> Response:
        """Get data by id.

        Arguments:
            event_id {int} -- id of the event

        Returns:
            Response -- A flask.Response containing the Event
        """

        event = self.__get_event(event_id)
        response = None

        if event:
            response = jsonify(
                {
                    "ok": True,
                    "status": "event/get-success",
                    "message": "Event has been successfully fetched.",
                    "event": model_to_dict(event),
                }
            )
        else:
            response = jsonify(
                {
                    "ok": False,
                    "status": "event/event-not-found",
                    "message": f"Event with id {event_id} does not exist.",
                }
            )
            response.status_code = 404

        return response

    def create(self) -> Response:
        """Create a new Event

        Returns:
            Response -- A flask.Response containing the created Event
        """

        data = EventPOSTData(request.get_json())
        event = self.__create_event(data)

        response = jsonify(
            {
                "ok": True,
                "status": "event/create-success",
                "message": "Event has been successfully created.",
                "event": model_to_dict(event),
            }
        )
        response.status_code = 201

        return response

    def __list_event(self) -> List[Event]:
        try:
            return Event.select().execute()  # pylint: disable=no-value-for-parameter
        except PeeweeException:
            raise RequestError(
                500, "event/list-error", "An error occured when trying to list Event."
            )

    def __get_event(self, event_id: int) -> Union[Event, None]:
        try:
            return Event.get_by_id(int(event_id))

        except PeeweeException:
            raise RequestError(
                500, "event/get-error", "An error occured when trying to get Event."
            )

        except ValueError:
            return None

    def __create_event(self, data: EventPOSTData) -> Event:
        try:
            return Event.create(recipients=data.recipients, category=data.category)
        except PeeweeException:
            raise RequestError(
                500,
                "event/create-error",
                "An error occured when trying to create Event.",
            )
