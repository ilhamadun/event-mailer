# -*- coding: utf-8 -*-
"""
    mailer.api.app
    ~~~~~~~~~~~~~~

    Main entrypoint of the Event Mailer API.

    Start the development server:
        FLASK_APP=mailer/api/app.py flask run

    :copyright: © 2018 by Ilham Imaduddin.
"""

from flask import Flask

from mailer.api.event import EventHandler
from mailer.api.email import SaveEmailHandler
from mailer.models import db, Email, Event

app = Flask(__name__)  # pylint: disable=invalid-name


@app.cli.command("initdb")
def init_db():
    """Initialize database tables."""
    db.connect()
    db.create_tables([Email, Event])
    db.close()


@app.route("/event", methods=["GET", "POST"])
@app.route("/event/<event_id>", methods=["GET"])
def event(event_id=None):
    """Create, get and list Event."""
    return EventHandler.handle_request(event_id)


@app.route("/save_emails", methods=["POST"])
def save_emails():
    """Save and enqueue email for sending."""
    return SaveEmailHandler.handle_request()
