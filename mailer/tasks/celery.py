# -*- coding: utf-8 -*-
"""
    mailer.tasks.celery
    ~~~~~~~~~~~~~~~~~~~

    Setup Celery instance to run task asynchronousy.

    :copyright: © 2018 by Ilham Imaduddin.
"""

import os

from celery import Celery

from mailer.tasks.email_sender import EmailSender

app = Celery(  # pylint: disable=invalid-name
    main="mailer-tasks", broker=os.environ["CELERY_BROKER"]
)


@app.task
def send_email(email_id: int, recipient_id: int):
    """Backgroundtask for sending email."""
    with EmailSender() as postman:
        postman.send(email_id, recipient_id)


if __name__ == "__main__":
    app.start()
