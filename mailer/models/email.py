# -*- coding: utf-8 -*-
"""
    mailer.models.email
    ~~~~~~~~~~~~~~~~~~~

    Email database model.

    :copyright: © 2018 by Ilham Imaduddin.
"""

from datetime import datetime

from peewee import CharField, ForeignKeyField, TextField
from playhouse.postgres_ext import DateTimeTZField

from mailer.models.db import BaseModel
from mailer.models.event import Event


class Email(BaseModel):  # pylint: disable=too-few-public-methods
    """Stores the details of an email."""

    event = ForeignKeyField(Event, backref="emails")
    subject = CharField()
    content = TextField()
    created_at = DateTimeTZField(default=datetime.now)
    send_at = DateTimeTZField()
