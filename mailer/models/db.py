# -*- coding: utf-8 -*-
"""
    mailer.models.db
    ~~~~~~~~~~~~~~~~

    Define the database to be used within models across the application.

    :copyright: © 2018 by Ilham Imaduddin.
"""

import os

from peewee import PostgresqlDatabase, Model

db = PostgresqlDatabase(  # pylint: disable=invalid-name
    os.environ["DB_NAME"],
    user=os.environ["DB_USER"],
    password=os.environ["DB_PASSWORD"],
    host=os.environ.get("DB_HOST", "localhost"),
    port=os.environ.get("DB_PORT", 5432),
)


class BaseModel(Model):  # pylint: disable=too-few-public-methods
    """BaseModel to be extended by all database models."""

    class Meta:  # pylint: disable=missing-docstring, too-few-public-methods, old-style-class, no-init
        database = db
