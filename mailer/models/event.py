# -*- coding: utf-8 -*-
"""
    mailer.models.event
    ~~~~~~~~~~~~~~~~~~~

    Event database model.

    :copyright: © 2018 by Ilham Imaduddin.
"""

from datetime import datetime

from peewee import CharField
from playhouse.postgres_ext import DateTimeTZField, ArrayField

from mailer.models.db import BaseModel


class Event(BaseModel):  # pylint: disable=too-few-public-methods
    """Stores the details of an event."""

    recipients = ArrayField(CharField)
    category = CharField()
    created_at = DateTimeTZField(default=datetime.now)
