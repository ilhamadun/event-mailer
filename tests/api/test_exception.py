# -*- coding: utf-8 -*-
"""
    tests.api.test_exception
    ~~~~~~~~~~~~~~~~~~~~~~~~

    Test cases for exceptions.

    :copyright: © 2018 by Ilham Imaduddin.
"""

import tests.context  # pylint: disable=unused-import
from mailer.api.app import app
from mailer.api.exception import RequestError


def test_response():
    """RequestError should produce a flask.Response."""
    with app.app_context():
        error = RequestError(400, "validation-error", "data is invalid")
        response = error.response()
        response_data = response.get_json()

        assert response.status_code == 400
        assert response_data["ok"] is False
        assert response_data["status"] == "validation-error"
        assert response_data["message"] == "data is invalid"
