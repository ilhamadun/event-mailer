# -*- coding: utf-8 -*-
"""
    tests.api.client
    ~~~~~~~~~~~~~~~~

    Flask test client.

    :copyright: © 2018 by Ilham Imaduddin.
"""
# pylint: disable=no-self-use,redefined-outer-name

import pytest

from mailer.api.app import app


@pytest.fixture
def client():
    """Flask test client."""
    app.config["TESTING"] = True
    return app.test_client()
