# -*- coding: utf-8 -*-
"""
    tests.api.test_email
    ~~~~~~~~~~~~~~~~~~~~

    Test cases for Send Email API handler.

    :copyright: © 2018 by Ilham Imaduddin.
"""
# pylint: disable=no-self-use,redefined-outer-name

from datetime import datetime
from unittest.mock import MagicMock

import pytest
from dateutil.parser import parse
from peewee import IntegrityError, PeeweeException

import tests.context  # pylint: disable=unused-import
from tests.api.client import client  # pylint: disable=unused-import
from mailer.api.email import SaveEmailData
from mailer.api.exception import RequestError
from mailer.models import Email, Event
from mailer.tasks.celery import send_email


class TestSaveEmailData(object):
    """Test cases for SaveEmailData."""

    def test_valid_data(self):
        """SaveEmailData should contains the same data with it's raw data."""
        raw_data = {
            "event_id": 1,
            "email_subject": "Email Subject",
            "email_content": "Email Content",
            "timestamp": datetime.now().isoformat(),
        }

        data = SaveEmailData(raw_data)

        assert data.event_id == raw_data["event_id"]
        assert data.email_subject == raw_data["email_subject"]
        assert data.email_content == raw_data["email_content"]
        assert data.timestamp == raw_data["timestamp"]

    def test_invalid_data(self):
        """Invalid data should raise RequestError."""
        raw_data = {
            "event_id": 1,
            "email_subject": "Email Subject",
            "email_content": "Email Content",
        }

        with pytest.raises(RequestError) as execinfo:
            SaveEmailData(raw_data)

        assert execinfo.value.code == 400
        assert execinfo.value.status == "save_emails/validation-error"
        assert "timestamp" in execinfo.value.message


@pytest.fixture
def request_data():  # pylint: disable=missing-docstring
    return {
        "event_id": 1,
        "email_subject": "Email Subject",
        "email_content": "Email Content",
        "timestamp": datetime.now().isoformat(),
    }


class TestSaveEmailHandler(object):
    """Test Cases for SaveEmailHandler."""

    def test_save_emails(self, client, request_data):
        """Should save and schedule email for sending."""

        event = Event(id=1, recipients=["some@email.com"])
        email = Email(
            id=1,
            subject=request_data["email_subject"],
            content=request_data["email_content"],
            send_at=request_data["timestamp"],
            event=event,
        )
        Email.create = MagicMock(return_value=email)
        send_email.apply_async = MagicMock()

        response = client.post("/save_emails", json=request_data)
        response_data = response.get_json()

        assert Email.create.call_args[1]["event_id"] == request_data["event_id"]
        assert Email.create.call_args[1]["subject"] == request_data["email_subject"]
        assert Email.create.call_args[1]["content"] == request_data["email_content"]
        assert Email.create.call_args[1]["send_at"] == request_data["timestamp"]
        assert send_email.apply_async.call_args[0][0] == (1, 0)
        assert send_email.apply_async.call_args[1]["eta"] == parse(email.send_at)

        assert response.status_code == 200
        assert response_data == {
            "ok": True,
            "status": "save_emails/success",
            "message": f"The email has been scheduled to be sent at {request_data['timestamp']}.",
        }

    def test_event_not_exist(self, client, request_data):
        """Save emails with invalid event id should return bad request."""
        Email.create = MagicMock(side_effect=IntegrityError())

        response = client.post("/save_emails", json=request_data)
        response_data = response.get_json()

        assert response.status_code == 400
        assert response_data == {
            "ok": False,
            "status": "save_emails/event-not-exist",
            "message": f"Event with id {request_data['event_id']} does not exist.",
        }

    def test_internal_error(self, client, request_data):
        """Internal error should return 500."""
        Email.create = MagicMock(side_effect=PeeweeException())

        response = client.post("/save_emails", json=request_data)
        response_data = response.get_json()

        assert response.status_code == 500
        assert response_data == {
            "ok": False,
            "status": "save_emails/save-error",
            "message": "An error occured when trying to save email.",
        }
